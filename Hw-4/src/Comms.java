interface Command {
    String getName();
    void exec();
}