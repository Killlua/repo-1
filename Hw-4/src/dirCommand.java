public class dirCommand implements Command{
    @Override
    public String getName() {
        return "dir";
    }

    @Override
    public void exec() {
        String dir = System.getProperty("user.dir");
        System.out.println(dir);
    }
}
