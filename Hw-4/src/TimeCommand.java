import java.sql.Time;
import java.time.Clock;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TimeCommand implements Command{
    @Override
    public String getName() {
        return "time";
    }

    @Override
    public void exec() {
        LocalTime t = LocalTime.now();
        LocalTime time = LocalTime.ofSecondOfDay(t.toSecondOfDay());
        System.out.println(time);
    }
}
