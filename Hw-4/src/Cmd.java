import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Cmd {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        List<Command> commands = new ArrayList<>();
        boolean add = commands.add(new DateCommand());
        commands.add(new TimeCommand());
        commands.add(new dirCommand());

        while (true) {
            System.out.print(">>");
            String cmd = scanner.nextLine();

            if ("exit".contentEquals(cmd)) {
                break;
            }

            for(Command command: commands) {
                if (command.getName().contentEquals(cmd)){
                    command.exec();
                }
            }
        }
    }
}
